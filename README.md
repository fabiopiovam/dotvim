# dotvim

Algumas configurações do `vim` que estou aprendendo

## Install

```sh
rm -rf ~/.vim*
ln -s ~/.dotfiles/dotvim/.vimrc ~/
ln -s ~/.dotfiles/dotvim ~/.vim
```
