# VIM - HowTo

## HowTos:
* Com vários arquivos abertos, como destacar o arquivo em foco ?
* Com vários arquivos abertos, como navegar diretamente para algum ?
* Tema com transparencia
* Comportamento com backup .swp
* Encoding by project
* Tabs by language
* NERDTree: Abrir arquivos com `sudo`
  `sudo vim [arquivo]` ?

* NERDTree: Estudar!

* Workspace
* plug-map ?
* definição de atalhos ?
* identificar quando um arquivo está aberto e não abrir, mas focar.
* Procurar palavra dentro de um diretorios

## Configuração do VIM
* Edite ou crie o arquivo `~/.vimrc`

## Modos:
* Command `ESC`
* Visual  `v`
* Insert  `i` (ToDo: verificar diferença com `I`)

## Commands
* Sair: `:q!`
* Salvar: `:w!`
* Salvar e sair: `:x!`

### Navegação do arquivo
* Posições: Left down up right - `h j k l` 
* Percorrer arquivo: `{num} + posição`
* Topo do arquivo `gg`
* Final do arquivo `G`
* Abrir para inserção na próxima linha `o` para anterior `O`
* Abrir para inserção ao final da linha: `A`
* Navegando por palavras: `w` para avançar e `b` para voltar
* Abrir sugestões de palavras já utilizadas no arquivo: `Ctrl + n`

### Navegação entre arquivos
* Dividir vertical e abrir arquivo a esquerda: `:vs {path/do/arquivo}`
* Dividir horizontal e abrir arquivo acima: `:sp {path/do/arquivo}`
* Navegar corrido entre arquivos: `Ctrl + ww`

### Procurar & substituir
* Procurar palavra `/{palavra-chave} + Enter`
	* utilize `n` para o próximo resultado e `N` para o resultado anterior

* Recortar a linha: `dd`

* Para alterar caracteres, selecione um ou vários em modo visual,  digite `r` em cima e depois o novo caracter

* Para recortar caracteres, selecione um ou vários em modo visual,  digite `x` em cima.

* Corrigir indentacao da linha `==`

* Substituir: `:%s/{o que procura}/{alterar para}/{options}`
	* options:
		* `g` - global
		* `c` - choice (perguntar sobre alterações)

### Recording para criar atalhos personalizados
* Para gravar determinada instrução: `q` e na sequencia uma letra que identifique
* realizar sequencia de comandos e inserts desejável
* finalize a gravação com `q`. Para testar basta digitar `@{letra escolhida}`

## Pluins

### NERDTree
Arvore de navegação de arquivos:
* `NERDTree`
* `s` para abrir um arquivo, dividindo verticalmente
* Sair do NERDTree: `:q` ou simplesmente `q`
* Abrir arquivos em abas: `t`
* Navegar entre as abas: `gt`
* Mostrar arquivos ocultos `I`
* CRUD files: `m` [selecionar a opção]

### Emmet
Semelhante ao `zencode` para HTML no VIM
```
div.container.panel-group>div.panel.panel-default*10>div.panel-heading+div.panel-body
```
* Na sequencia, digite via command `Ctrl+y` e depois `,`
	* Comportamento alterado no `~/.vimrc` para ação ocorrer usando `,,`
	(veja a documentação do Emmet)

Fold code in Vim
`za` : Toggle code folding at the current line. The block that the current line belongs to is folded (closed) or unfolded (opened).
`zo` : Open fold.
`zc` : Close fold.
`zR` : Open all folds.
`zM` : Close all folds.

